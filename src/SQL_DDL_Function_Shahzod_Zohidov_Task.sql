create view revenue_by_category as
select c.name as cat_name, c.category_id as cat_id, sum(p.amount) as total_rev
from payment p
join rental r on p.rental_id = r.rental_id
join inventory i on r.inventory_id = i.inventory_id
join film_category fc on i.film_id = fc.film_id
join category c on fc.category_id = c.category_id
where extract(quarter from p.payment_date) = extract(quarter from current_date)
group by  c.name, c.category_id
having sum(p.amount) > 0
order by total_rev desc;

create or replace function get_sales_revenue_by_category_qtr(text)
returns table (category_id smallint,category_name text,total_revenue numeric)as $$
declare start_date date; end_date date;
begin
    start_date := to_date($1, 'YYYY-"Q"Q');
    end_date := start_date + interval '3 months';
    return query
    select fc.category_id,cat.name as category_name, sum(p.amount) as total_revenue
    from payment p
    join rental r on p.rental_id = r.rental_id
    join inventory i on r.inventory_id = i.inventory_id
    join film_category fc on i.film_id = fc.film_id
    join category cat on fc.category_id = cat.category_id
    where p.payment_date >= start_date and p.payment_date < end_date
    group by fc.category_id, cat.name
    having sum(p.amount) > 0
    order by total_revenue desc;
end;
$$ language plpgsql;

create or replace function new_movie(movie_title text)
returns void as $$
declare new_film_id INT;
BEGIN
    select max(film_id) + 1 into new_film_id from film;
    if not exists (select 1 from language where name = 'Klingon') THEN
        raise exception 'Language Klingon doesnt exists in the table.';
    end if;
    insert into film(film_id, title, rental_rate, rental_duration, replacement_cost, release_year, language_id)
    values(new_film_id, movie_title, 4.99, 3, 19.99, extract(year from current_date), 
           (select language_id from language where name = 'Klingon'));
end ;
$$ language plpgsql;